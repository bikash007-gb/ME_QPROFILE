
module.exports = {
  // TODO: change your profile information here
  name: "Bikash Mali",
  greeting: "Hey 👋",
  greetingDescription: "I'm Bikash Mali and I'm a Software Engineer!",
  githubUrl: "https://github.com/bikash007-gb",
  linkedinUrl: "https://www.linkedin.com/in/bikash-mali-6629b7192/",
  cvLink: "https://docs.google.com/document/d/1o5FRk-dqERDGX6FijYgyx6fVR4arv_ezOnyPGyqUtQU/edit",
};
